echo "# Rio Game Assets" > README.md
echo "This repository contains all the assets for the Rio game project." >> README.md
echo "## Directory Structure" >> README.md
echo "- images/sprites: Sprite images for characters and objects" >> README.md
echo "- sounds/effects: Sound effects" >> README.md
echo "- sounds/music: Background music" >> README.md
